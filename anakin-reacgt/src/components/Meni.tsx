import React from "react";
import { Proizvod, TipProizvoda } from "../models/Proizvod";
import ProizvodView from "./ProizvodView";
import { RouteComponentProps } from "react-router-dom";
import { RootStanje } from "../store";
import { connect } from "react-redux";
import { A_ZahtevZaUcitavanje } from "../store/proizvodi/Akcije";

interface Props
{
    proizvodi: Proizvod[],
    idjeviNarucenihProizvoda: string[],//krpljenje kako bi upamtio cekirane proizvode kad menjam rute, znaci izvucem id odavde i kazem da je cekiran
    podaciSeUcitavaju: boolean,
    adminJePrijavljen: boolean,
    usernameAdmina: string,
    tokenAdmina: string
}

interface ActionProps
{
    ucitajProizvode: (kategorijaProizvoda: string) => void
}

type KompletanProps = Props & ActionProps & RouteComponentProps<{kategorijeProizvoda:string}>;

class Meni extends React.Component<KompletanProps>
{
    componentDidMount(): void
    {
        const { kategorijeProizvoda } = this.props.match.params;
        let filterKriterijum = this.premapirajKategoriju(kategorijeProizvoda);
        
        this.props.ucitajProizvode(filterKriterijum);
    }

    render(): JSX.Element 
    {
        const { podaciSeUcitavaju } = this.props;
        return(
            <React.Fragment>
                <button className="btn btn-outline-warning btn-block" 
                        onClick={this.idiNazad}>
                        <h3>Nazad na ponudu</h3>
                </button>
                {
                    podaciSeUcitavaju ? 
                        <h1 className="col-sm-6 offset-sm-3 text-center"> Loading </h1>   
                        : this.izrenderujProizvode()
                }
            </React.Fragment>
        );
    }

    izrenderujProizvode = (): JSX.Element[] | JSX.Element => {
        const { proizvodi, adminJePrijavljen, tokenAdmina, usernameAdmina } = this.props;
        
        if(proizvodi && proizvodi.length === 0)
            return (<h1 className="col-sm-6 offset-sm-3 text-center">
                        Nema nijednog proizvoda u ponudi
                    </h1>);
        else
           return (proizvodi.map((proizvod: Proizvod, redniBrElementa: number) => {
                return <ProizvodView key={redniBrElementa}
                                     proizvodJeCekiran={this.proveriNarucenost(proizvod._id)} 
                                     proizvod={proizvod}
                                     adminJePrijavljen={adminJePrijavljen}
                                     usernameAdmina={usernameAdmina}
                                     tokenAdmina={tokenAdmina}/>
            }));
    }

    idiNazad = (): void => {
        this.props.history.push("/naruci");
    }

    premapirajKategoriju = (kategorija: string): string => {
        switch (kategorija)
        {
            case "hladniNapici": return TipProizvoda.HLADNI_NAPITAK;
            case "topliNapici": return TipProizvoda.TOPLI_NAPITAK;
            case "kolaci": return TipProizvoda.KOLAC;

            default: return TipProizvoda.HLADNI_NAPITAK;
        }
        
    }

    proveriNarucenost = (indeksElementa: string): boolean => {
        return this.props.idjeviNarucenihProizvoda.includes(indeksElementa);
    }
}

const mapStateToProps = (rootStanje: RootStanje) : Props => 
{
    const { stanjeProizvoda, informacijeAdmin, stanjeNarudzbine } = rootStanje;
    return{
        proizvodi: stanjeProizvoda.proizvodi,
        podaciSeUcitavaju: rootStanje.stanjeProizvoda.proizvodiSeUcitavaju,
        adminJePrijavljen: informacijeAdmin.adminJePrijavljen,
        usernameAdmina: informacijeAdmin.adminPodaci.username,
        tokenAdmina: informacijeAdmin.adminPodaci.token,
        idjeviNarucenihProizvoda: stanjeNarudzbine.naruceniProizvodi.map(stavka => {return stavka.narucenProizvod._id })
    }
}

const mapDispatchToProps = (dispatch: any) : ActionProps => 
{
    return {
        ucitajProizvode : (kategorija: string) => dispatch(A_ZahtevZaUcitavanje(kategorija))
    }
}


export default connect(mapStateToProps,mapDispatchToProps) (Meni);