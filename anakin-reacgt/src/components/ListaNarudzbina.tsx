import React from "react";
import { StavkaRacuna } from "../models/StavkaRacuna";
import NarudzbinaView from "./NarudzbinaView";
import { RootStanje } from "../store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { A_PotvrdiNarudzbinu } from "../store/narudzbine/Akcije";

interface Props { stavkeNaRacunu: StavkaRacuna[]; };
interface ActionProps { potvrdiNarudzbinu: (stavkeNaNarudzbini: StavkaRacuna[]) => void; }

class ListaNarudzbina extends React.Component<Props & ActionProps>
{
    render(): JSX.Element
    {
        const { stavkeNaRacunu } = this.props;
        return (
            <div className="col-sm-6 offset-sm-3 text-center">
                <h2>Račun</h2>
                {
                    stavkeNaRacunu && stavkeNaRacunu.map((stavka, redniBr) => { 
                        return <NarudzbinaView key={redniBr} 
                                               narudzbina={stavka}/>
                    })
                }
                <h2>Ukupno: {this.preracunajCenu(this.props.stavkeNaRacunu)} RSD</h2>
                <button className="btn btn-primary btn-lg"
                        onClick={this.hendlujKlik}
                        disabled={stavkeNaRacunu.length === 0}>
                        Potvrdi narudžbinu
                </button>
            </div>
        );
    }

    hendlujKlik = (): void => {
        this.props.potvrdiNarudzbinu(this.props.stavkeNaRacunu);
    }

    preracunajCenu = (stavkeNaRacunu: StavkaRacuna[]): string => {
        let racunValjda = stavkeNaRacunu.reduce((akumulator, stavkaRacuna) => {
            return akumulator + stavkaRacuna.kolicina * (stavkaRacuna.narucenProizvod.cena * (1 - stavkaRacuna.narucenProizvod.akcija / 100));
        }, 0);

        return racunValjda.toFixed(2);
    }
}

const mapStateToProps = (rootStanje: RootStanje): Props => {
    const { stanjeNarudzbine } = rootStanje;
    return {
        stavkeNaRacunu: stanjeNarudzbine.naruceniProizvodi
    };
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        potvrdiNarudzbinu: (stavkeNaNarudzbini) => dispatch(A_PotvrdiNarudzbinu(stavkeNaNarudzbini))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListaNarudzbina);