import React from "react";
import { Admin } from "../models/Admin";
import { LoginGreske } from "../models/LoginGreske";
import { RootStanje } from "../store";
import { Dispatch } from "redux";
import { A_PrijavljivanjeAdmina } from "../store/admini/Akcije";
import { connect } from "react-redux";
import { RouteComponentProps, Redirect } from "react-router-dom";

interface Props
{
    prijavaSeObradjuje: boolean;
    greske: LoginGreske;
    adminJeUlogovan: boolean;
};

interface ActionProps
{
    ulogujAdmina: (loginPodaci: Admin) => void;
};

interface State 
{
    username: string;
    password: string;
};

class AdminLoginPage extends React.Component<Props & ActionProps & RouteComponentProps, State>
{
    readonly state: State = {
        username: "",
        password: ""
    };

    render(): JSX.Element
    {
        const { prijavaSeObradjuje, greske, adminJeUlogovan } = this.props;
        
        if(adminJeUlogovan === true)
        {
            alert("Uspešno prijavljivanje");
            return <Redirect exact to="/naruci" />;
        }

        return (
            <div className="col-sm-6 offset-sm-3 text-center">
                <h1>Login admina</h1>
                {
                    prijavaSeObradjuje && this.prikaziObradjivanje()
                }
                <div className="form-group">
                    <label className="control-label" style={{fontWeight: "bold"}}>
                        Username:
                    </label>
                    <input type="text" 
                           name="username" 
                           placeholder="Unesi username" 
                           className="form-control"
                           style={{textAlign: "center"}}
                           onChange={this.onChangeInput}/>
                    {
                        greske.usernameNePostoji && 
                        <p style={{color: 'red'}}>Nepostojeće korisničko ime</p>
                    }
                    
                    <label className="control-label" style={{fontWeight: "bold"}}>
                        Lozinka:
                    </label>
                    <input type="password" 
                           name="password" 
                           placeholder="Unesi lozinku" 
                           className="form-control"
                           style={{textAlign: "center"}}
                           onChange={this.onChangeInput}/>
                    {
                        greske.passwordJePogresan &&
                        <p style={{color: 'red'}}>Loznika je netačna</p>
                    }

                    <button className="btn btn-primary btn-lg"
                            onClick={this.prijaviSe}
                            disabled={prijavaSeObradjuje}>
                        Prijavi se
                    </button>
                </div>
            </div>
        );
    };

    prikaziObradjivanje = (): JSX.Element => {
        return <h4 className="col-sm-6 offset-sm-3 text-center"
                    style={{color: "orange"}}>
                    Prijava se obrađuje
                </h4>
    };

    onChangeInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ [event.target.name] : event.target.value} as Pick<State, any>);
    };

    prijaviSe = (): void => {
        let loginPodaci: Admin = {
            username: this.state.username,
            password: this.state.password,
            token: "Ovo ce biti prazno, pa cu sa servera da to dobijem kad uspe login, i cuvacu ga takvog"
        };

        this.props.ulogujAdmina(loginPodaci);
    };

    //nesto fali ovde, bem ga, pokusavam sad sa prijavaJeUspela
    nemaGresaka = (greske: LoginGreske): boolean => {
        const { usernameNePostoji: usernameJePogresan, passwordJePogresan, tokenNeValja } = greske;
        return !(usernameJePogresan && passwordJePogresan);
    };
}

const mapStateToProps = (rootStanje: RootStanje): Props => {
    const { informacijeAdmin } = rootStanje;
    return {
        greske: informacijeAdmin.greske,
        prijavaSeObradjuje: informacijeAdmin.prijavaSeObradjuje,
        adminJeUlogovan: informacijeAdmin.adminJePrijavljen
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        ulogujAdmina: (loginPodaci: Admin) => dispatch(A_PrijavljivanjeAdmina(loginPodaci))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminLoginPage);