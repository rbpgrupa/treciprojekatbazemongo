import React from "react";
import { Proizvod, TipProizvoda } from "../models/Proizvod";
import { RootStanje } from "../store";
import { Dispatch } from "redux";
import { A_DodavanjeProizvoda } from "../store/proizvodi/Akcije";
import { connect } from "react-redux";
import { ZahtevDodavanjaProizvoda } from "../models/ZahtevDodavanjeProizvoda";

interface Props
{
    adminUsername: string,
    adminToken: string,
    adminJePrijavljen: boolean;
};

interface ActionProps
{
    dodajNoviProizvod: (zahtevDodavanja: ZahtevDodavanjaProizvoda) => void
}

interface State
{
    naziv: string;
    cena: number;
    tipProizvoda: TipProizvoda;
}

class DodavanjeProizvoda extends React.Component<Props & ActionProps, State>
{
    readonly state: State = {
        cena: 0,
        naziv: "",
        tipProizvoda: TipProizvoda.HLADNI_NAPITAK
    };

    render(): JSX.Element
    {
        return (
            <div className="col-sm-6 offset-sm-3 text-center">
                <h1>Unesi novi proizvod</h1>
                <div className="form-group">
                    <label className="control-label">Naziv:</label>
                        <input type="text"
                               name="naziv"
                               onChange={this.hendlujUnos}/>
                        <hr/>
                    <label className="control-label">Cena: </label>
                        <input type="number"
                               name="cena"
                               onChange={this.hendlujUnos}/>
                        <hr/>
                    <h5 className="control-label">Odaberi tip: </h5>
                        <input type="radio"
                               //checked *** ako ovo stavim, stalno cekira ovo dugme, nezavisno od izbora (mozda zbog setState...)
                               name="RadioButtonTipProizvoda"
                               value={TipProizvoda.HLADNI_NAPITAK}
                               onChange={this.hendlujTipProizvoda} />
                               Hladni napitak
                        <hr/>
                        <input type="radio"
                               value={TipProizvoda.TOPLI_NAPITAK}
                               name="RadioButtonTipProizvoda"
                               onChange={this.hendlujTipProizvoda}/>
                                Topli napitak
                        <hr/>
                        <input type="radio"
                               value={TipProizvoda.KOLAC}
                               name="RadioButtonTipProizvoda"
                               onChange={this.hendlujTipProizvoda}/>
                                Kolac
                        <hr/>
                        <button className="btn btn-primary btn-lg"
                            onClick={this.hendlujDodavanje}>
                                Dodaj proizvod
                    </button>
                </div>
            </div>
        );
    }

    hendlujUnos = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ [event.target.name] : event.target.value } as Pick<State, any>);
    }

    hendlujTipProizvoda = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ tipProizvoda: event.target.value as TipProizvoda});
    }

    hendlujDodavanje = (): void => {
        let noviProizvod = {
            _id: "",//baza mu sama dodeljuje nesto
            naziv: this.state.naziv,
            cena: +this.state.cena,
            tip: this.state.tipProizvoda,
            akcija: 0 //novi proizvod nema akciju, posle ce to da se dodaje
        };

        let noviZahtev: ZahtevDodavanjaProizvoda = {
            username: this.props.adminUsername,
            token: this.props.adminToken,
            proizvod: noviProizvod
        };
        console.table(noviProizvod);

        this.props.dodajNoviProizvod(noviZahtev);
    }
};

const mapStateToProps = (rootStanje: RootStanje): Props => {
    const { informacijeAdmin } = rootStanje;
    return {
        adminUsername: informacijeAdmin.adminPodaci.username,
        adminToken: informacijeAdmin.adminPodaci.token,
        adminJePrijavljen: informacijeAdmin.adminJePrijavljen
    };
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        dodajNoviProizvod: (zahtevDodavanja: ZahtevDodavanjaProizvoda) => dispatch(A_DodavanjeProizvoda(zahtevDodavanja))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DodavanjeProizvoda);