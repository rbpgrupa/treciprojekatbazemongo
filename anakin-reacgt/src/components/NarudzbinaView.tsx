import React from 'react'
import { StavkaRacuna } from '../models/StavkaRacuna'

interface Props { narudzbina: StavkaRacuna; };

class NarudzbinaView extends React.Component<Props> 
{
    render(): JSX.Element
    {
        const { kolicina, narucenProizvod } = this.props.narudzbina;
        return (
            <React.Fragment>
                <div className="col-sm-6 offset-sm-2 text-center">
                    <div className="form-group">
                        {narucenProizvod.naziv}, {(narucenProizvod.cena * (1 - narucenProizvod.akcija / 100)).toFixed(2)} RSD  X {kolicina}
                    </div>
                    <hr/>    
                </div>
            </React.Fragment>
        );
    }
}

export default NarudzbinaView