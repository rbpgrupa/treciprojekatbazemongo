import React from 'react'
import { Link } from 'react-router-dom'

class Ponuda extends React.Component<{}> 
{
    render(): JSX.Element
    {
        return(
            <div>
                <Link to="/naruci/topliNapici" 
                      className="col-sm-6 offset-sm-3 text-center">
                        <h3>Topli Napici</h3>
                </Link> <hr/>
                <Link to="/naruci/hladniNapici"
                      className="col-sm-6 offset-sm-3 text-center">
                        <h3>Hladni Napici</h3>
                </Link> <hr/>
                <Link to="/naruci/kolaci"
                      className="col-sm-6 offset-sm-3 text-center">
                        <h3>Kolači</h3>
                </Link> <hr/>
            </div>
        )
    }
    
}




export default Ponuda