import React from "react";
import { Proizvod } from "../models/Proizvod";
import { A_CekiranjeProizvoda, A_OdcekiranjeProizvoda, A_PromeniKolicinuProizvoda } from "../store/narudzbine/Akcije";
import { StavkaRacuna } from "../models/StavkaRacuna";
import { connect } from "react-redux";
import { A_BrisanjeProizvoda, A_DodavanjeAkcijeProizvodu } from "../store/proizvodi/Akcije";

interface Props 
{
    proizvod: Proizvod,
    proizvodJeCekiran: boolean,
    adminJePrijavljen: boolean,
    usernameAdmina: string,
    tokenAdmina: string
}

interface State
{
    kolicinaProizvoda: number;
    iznosAkcije: number;
}

interface ActionProps
{
    cekirajProizvod: (stavkaRacuna: StavkaRacuna) => void;
    odcekirajProizvod: (stavkaRacuna: StavkaRacuna) => void;
    promeniKolicinu: (stavkaRacuna: StavkaRacuna) => void;
    dodajAkciju: (idProizvoda: string, username: string, token: string, iznosAkcije: number) => void;
    obrisiProizvod: (idProizvoda: string, username: string, token: string) => void
}

class ProizvodView extends React.Component<Props & ActionProps, State>
{
    readonly state : State = {
        kolicinaProizvoda: 1,
        iznosAkcije: 0
    }
    
    render():JSX.Element 
    {
        const { adminJePrijavljen, proizvodJeCekiran } = this.props;
        const { cena, naziv, akcija } = this.props.proizvod;
        const { kolicinaProizvoda, iznosAkcije } = this.state;
        return(
            <div className="col-sm-6 offset-sm-2 text-center">
                <input type="checkbox"
                       checked={proizvodJeCekiran} 
                       onChange={this.cekirajProizvod}/>
                       {naziv}, {this.prikaziCenu()} <br/>
                    Količina: <input type="number" 
                                     onChange={this.postaviKolicinu}
                                     value={kolicinaProizvoda}/>
                    <button onClick={this.izbrisiProizvod}
                            className="btn btn-danger"
                            hidden={!adminJePrijavljen}>
                            Obriši
                    </button>
                    <p hidden={!adminJePrijavljen}>Dodaj akciju</p>
                    <input type="number" 
                           onChange={this.hendlujAkciju} 
                           value={iznosAkcije}
                           hidden={!adminJePrijavljen}/>
                    <button onClick={this.dodajAkciju}
                            className="btn btn-danger"
                            hidden={!adminJePrijavljen}>
                            Dodaj akciju
                    </button>
                    <hr/>    
            </div>
        );
    }

    hendlujAkciju = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({iznosAkcije: parseInt(event.target.value)});
    }

    izbrisiProizvod = (): void =>{
        const { proizvod, usernameAdmina, tokenAdmina } = this.props;
        this.props.obrisiProizvod(proizvod._id, usernameAdmina, tokenAdmina);
    }

    cekirajProizvod = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const { proizvod } = this.props;
        const stavkaNaRacunu : StavkaRacuna = {
             narucenProizvod: proizvod,
             kolicina: this.state.kolicinaProizvoda
        }
        event.target.checked? this.props.cekirajProizvod(stavkaNaRacunu) :
                              this.props.odcekirajProizvod(stavkaNaRacunu);
    }

    dodajAkciju = (): void => {
        const { proizvod, usernameAdmina, tokenAdmina } = this.props;
        console.log(proizvod);
        this.props.dodajAkciju(proizvod._id, usernameAdmina, tokenAdmina, this.state.iznosAkcije);
    }

    postaviKolicinu = (event: React.ChangeEvent<HTMLInputElement>): void => {
        let vrednostIzInputa: number = parseInt(event.target.value);
        if(vrednostIzInputa < 1)
            vrednostIzInputa = 1;

        this.setState( { kolicinaProizvoda: vrednostIzInputa } );
        this.props.promeniKolicinu({narucenProizvod: this.props.proizvod, kolicina: vrednostIzInputa});
    }

    prikaziCenu = (): JSX.Element => {
        const { akcija, cena } = this.props.proizvod;
        if(akcija === 0)
            return (<span style={{textDecorationLine : 'none'}}>{cena.toFixed(2)}  RSD</span>)
        else
        {
            return (
                <React.Fragment>
                    <span style={{textDecoration: 'line-through'}}> {cena.toFixed(2)} </span>
                    <span style={{color: 'red'}}>{(cena * (1 - akcija/100)).toFixed(2) }, -{akcija}% </span>
                </React.Fragment>
            );
        }
    }
}

const mapDispatchToProps = (dispatch: any): ActionProps =>
{
    return {
        cekirajProizvod : (stavkaRacuna : StavkaRacuna) => dispatch(A_CekiranjeProizvoda(stavkaRacuna)),
        odcekirajProizvod: (stavkaRacuna: StavkaRacuna) => dispatch(A_OdcekiranjeProizvoda(stavkaRacuna)),
        promeniKolicinu: (stavkaRacuna: StavkaRacuna) => dispatch(A_PromeniKolicinuProizvoda(stavkaRacuna)),
        dodajAkciju: (idProizvoda: string, username: string, token: string, iznosAkcije: number) => dispatch(A_DodavanjeAkcijeProizvodu(idProizvoda, username, token, iznosAkcije)),
        obrisiProizvod: (idProizvoda: string, username: string, token: string) => dispatch(A_BrisanjeProizvoda(idProizvoda, username, token))
    }
}

export default connect (null,mapDispatchToProps) (ProizvodView);