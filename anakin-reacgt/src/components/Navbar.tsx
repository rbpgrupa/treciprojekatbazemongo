import React from 'react'
import { Link, NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
import { RootStanje } from '../store';
import { Dispatch } from 'redux';
import { A_OdjaviAdmina } from '../store/admini/Akcije';
import { connect } from 'react-redux';

interface Props
{
    adminJePrijavljen: boolean
}

interface ActionProps
{
    odjaviAdmina: () => void
}

type KompletanProps = Props & ActionProps & RouteComponentProps;

class Navbar extends React.Component<KompletanProps, {}> 
{
    render(): JSX.Element
    {
        const { adminJePrijavljen } = this.props;

        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-success">
                <Link className="navbar-brand ml-5" to="/">Café "Anakin en ʻKattenliefhebber"</Link>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav m-auto">
                            <li className="nav-item active">
                                <NavLink exact to="/" 
                                         activeClassName="btn btn-outline-warning" 
                                         className="nav-link text-white text-uppercase font-italic mr-5"> 
                                         Dobro došli 
                                </NavLink>
                            </li>
                            <li className="nav-item active">
                                <NavLink exact to="/naruci"
                                         className="nav-link text-white text-uppercase font-italic mr-5" 
                                         activeClassName="btn btn-outline-warning">
                                         Naruči
                                </NavLink>
                            </li>
                            <li className="nav-item active">
                                <NavLink exact to="/narudzbina"
                                         className="nav-link text-white text-uppercase font-italic mr-5" 
                                         activeClassName="btn btn-outline-warning">
                                         Vaša narudžbina 
                                </NavLink>
                            </li>
                            <li className="nav-item active">
                                <NavLink exact to="/adminLogin"
                                         className="nav-link text-white text-uppercase font-italic mr-5"
                                         activeClassName="btn btn-outline-warning"
                                         hidden={ adminJePrijavljen } >
                                        Prijavi se
                                </NavLink>
                            </li>
                            <li className="nav-item active">
                                <NavLink exact to="/dodajNoviProizvod"
                                         className="nav-link text-white text-uppercase font-italic mr-5"
                                         activeClassName="btn btn-outline-warning"
                                         hidden={ !adminJePrijavljen } >
                                        Dodaj novi proizvod
                                </NavLink>
                            </li>
                            <li className="nav-item active">
                                <NavLink exact to="/pogledajRacune"
                                         className="nav-link text-white text-uppercase font-italic mr-5"
                                         activeClassName="btn btn-outline-warning"
                                         hidden={ !adminJePrijavljen } >
                                        Pogledaj račune
                                </NavLink>
                            </li>
                            <li>
                                <button className="btn btn-danger"
                                        onClick={this.odjaviAdmina}
                                        hidden={!adminJePrijavljen}>
                                    Odjavi se
                                </button>
                            </li>
                    </ul>
                </div>
          </nav>  
        );
    }

    odjaviAdmina = (): void => {
        this.props.odjaviAdmina();
        alert("Uspešna odjava");
        this.props.history.push("/");
    }
}

const mapStateToProps = (rootStanje: RootStanje): Props => {
    const { informacijeAdmin } = rootStanje;
    return {
        adminJePrijavljen: informacijeAdmin.adminJePrijavljen
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        odjaviAdmina: () => dispatch(A_OdjaviAdmina())
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));