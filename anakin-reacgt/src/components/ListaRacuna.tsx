import React from "react";
import { CeoRacun } from "../models/CeoRacun";
import StavkaRacunaView from "./StavkaRacunaView";
import { RootStanje } from "../store";
import { Dispatch } from "redux";
import { A_UcitajRacune } from "../store/racuni/Akcije";
import { connect } from "react-redux";

interface Props
{
    sviRacuni: CeoRacun[],
    usernameAdmina: string,
    tokenAdmina: string,
    racuniSeUcitavaju: boolean,
}

interface ActionProps
{
    ucitajRacune: (username: string, token: string) => void;
}

class ListaRacuna extends React.Component<Props & ActionProps>
{
    componentDidMount()
    {
        this.props.ucitajRacune(this.props.usernameAdmina, this.props.tokenAdmina);
    }

    render(): JSX.Element
    {
        const { sviRacuni, racuniSeUcitavaju } = this.props;

        return (
            <div className="col-sm-6 offset-sm-2 text-center">
                <h2>Dosadašnji računi</h2>
                <div className="form-group">
                    {
                        racuniSeUcitavaju? <h1 className="col-sm-6 offset-sm-3 text-center"> Loading </h1> 
                                          : this.izrenderujRacune()
                    }
                </div>
            </div>
        );
    }

    izrenderujRacune = (): JSX.Element[] | JSX.Element => {
        const { sviRacuni } = this.props;
        
        if(sviRacuni && sviRacuni.length === 0)
            return (<h1 className="col-sm-6 offset-sm-3 text-center">
                        Nema nijednog proizvoda u ponudi
                    </h1>);
        else
           return (sviRacuni.map((racun: CeoRacun, redniBrElementa: number) => {
                return <StavkaRacunaView key={redniBrElementa}
                                         ceoRacun={racun}/>
            }));
    }
}

const mapStateToProps = (rootStanje: RootStanje): Props => {
    return {
        usernameAdmina: rootStanje.informacijeAdmin.adminPodaci.username,
        tokenAdmina: rootStanje.informacijeAdmin.adminPodaci.token,
        sviRacuni: rootStanje.informacijeRacuni.racuni,
        racuniSeUcitavaju: rootStanje.informacijeRacuni.racuniSeUcitavaju
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        ucitajRacune: (username, string) => dispatch(A_UcitajRacune(username, string))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListaRacuna);