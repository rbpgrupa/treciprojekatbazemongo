import React from 'react'
import kafa from '../slikeZaHome/kafaa.jpg'

class Pocetna extends React.Component<{}, {}>
{
    render(): JSX.Element
    {
        return (
            <div className="col-sm-6 offset-sm-2 text-center">
                <h1>Topla kafa, hladno piće, sveži kolači...</h1>
                <img src={kafa} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
            </div>
        );
    }
}

export default Pocetna