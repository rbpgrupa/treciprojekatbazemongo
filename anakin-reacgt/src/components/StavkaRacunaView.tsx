import React from "react";
import { CeoRacun } from "../models/CeoRacun";

//telo(veliki preformatiran string koji ima spisak svih proizvoda sa cenama i kolicinama)
//ukupna cena(number), datum porudzbine(string), 
interface Props
{
   ceoRacun: CeoRacun
}

class StavkaRacunaView extends React.Component<Props>
{
    render(): JSX.Element
    {
        const { telo, cena: ukupnaCena, datumPorudzbine } = this.props.ceoRacun;
        return (
            <div className="col-sm-6 offset-sm-2 text-center">
                <p>{telo}</p>
                <p>{ukupnaCena} RSD</p>
                <p>{datumPorudzbine}</p>
            </div>
        )
    }
}

export default StavkaRacunaView;