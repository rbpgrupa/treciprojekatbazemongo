import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css' ;
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Pocetna from './components/Pocetna'
import Ponuda from './components/Ponuda';
import Meni from './components/Meni';
import AdminLoginPage from './components/AdminLoginPage';
import Navbar from './components/Navbar';
import DodavanjeProizvoda from './components/DodavanjeProizvoda';
import ListaNarudzbina from './components/ListaNarudzbina';
import ListaRacuna from './components/ListaRacuna';

const App = () => {
  return (
    <div>
      <Router>
        <Navbar />
        <Route exact path='/' component={Pocetna} />
        <Route exact path='/naruci' component={Ponuda} />
        <Route exact path='/naruci/:kategorijeProizvoda' component={Meni}/>
        <Route exact path='/narudzbina' component={ListaNarudzbina} />
        <Route exact path='/adminLogin' component={AdminLoginPage} />
        <Route exact path='/dodajNoviProizvod' component={DodavanjeProizvoda} />
        <Route exact path='/pogledajRacune' component={ListaRacuna} />
      </Router>
    </div>
  );
}

export default App;
