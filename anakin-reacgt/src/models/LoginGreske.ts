//ovde ce da se nalazi model koji ce saga da vrati u reducer i u kome se nalaze sve moguce greske koje se 
//javljaju prilikom logina
export interface LoginGreske
{
    usernameNePostoji: boolean,
    passwordJePogresan: boolean,
    tokenNeValja: boolean //videcu dal ovo valja ovako...
}