import { LoginGreske } from "./LoginGreske";

export interface ResponseLogovanjaServer
{
    greske: LoginGreske,
    token: string
}