import { Proizvod } from "./Proizvod";

export interface ZahtevDodavanjaProizvoda
{
    username: string,
    token: string,
    proizvod: Proizvod
}