export enum TipProizvoda
{
    HLADNI_NAPITAK = "Hladni napitak",
    TOPLI_NAPITAK = "Topli napitak",
    KOLAC = "Kolac"
}
 /*dodati procente, moze i strike-through stil, akcija u procentima, a treba mi i 
 bool koji kaze dal je proizvod selektovan (ili da nekako premostim selekciju proizvoda)*/
export interface Proizvod
{
    _id: any,
    tip: TipProizvoda,
    naziv: string,
    cena: number,
    akcija: number //iznos akcije u procentima, 0 znaci da nema nikakve akcije
}