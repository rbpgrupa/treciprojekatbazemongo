import { Proizvod } from "./Proizvod";

export interface StavkaRacuna 
{
    narucenProizvod: Proizvod,
    kolicina: number
}