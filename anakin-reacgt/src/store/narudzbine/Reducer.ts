import { StavkaRacuna } from "../../models/StavkaRacuna";
import { Action } from "redux";
import { AkcijeSaNarudzbinama, CekiranjeProizvoda, OdcekiranjeProizvoda, PromeniKolicinuProizvoda } from "./Modeli";


export interface InformacijeONarduzbini
{
    naruceniProizvodi: StavkaRacuna[]
}

const pocetnoStanjeNaruzbine : InformacijeONarduzbini =
{
    naruceniProizvodi: []
}

export default function ReducerNarudzbine(stanje = pocetnoStanjeNaruzbine, akcija : Action <any> ) : InformacijeONarduzbini
{
    switch(akcija.type)
    {
        case AkcijeSaNarudzbinama.CEKIRANJE_PROIZVODA : 
        {
            const { stavkaKojaSeDodaje } = akcija as CekiranjeProizvoda;
            return{
                ...stanje,
                naruceniProizvodi: [...stanje.naruceniProizvodi, stavkaKojaSeDodaje]
            }
        }

        case AkcijeSaNarudzbinama.ODCEKIRANJE_PROIZVODA:
        {
            const { stavkaRacuna } = akcija as OdcekiranjeProizvoda;
            return {
                ...stanje,
                naruceniProizvodi: stanje.naruceniProizvodi.filter(stavka => stavka.narucenProizvod._id !== stavkaRacuna.narucenProizvod._id)
            }
        }

        case AkcijeSaNarudzbinama.PROMENI_KOLICINU_PROIZVODA:
        {
            const { stavka } = akcija as PromeniKolicinuProizvoda;
            return {
                ...stanje,
                naruceniProizvodi: stanje.naruceniProizvodi.map(stavkaRacuna => {
                    if(stavkaRacuna.narucenProizvod._id === stavka.narucenProizvod._id)
                    {
                        stavkaRacuna.kolicina = stavka.kolicina;
                        return stavkaRacuna;
                    }
                    else
                        return stavkaRacuna;
                })
            };
        }

        default:
             return stanje
    }
}