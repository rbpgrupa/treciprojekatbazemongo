import { PromeniKolicinuProizvoda , AkcijeSaNarudzbinama, CekiranjeProizvoda, OdcekiranjeProizvoda, PotvrdiNarudzbinu } from "./Modeli";
import { StavkaRacuna } from "../../models/StavkaRacuna";


export const A_CekiranjeProizvoda = (stavkaRacuna: StavkaRacuna) : CekiranjeProizvoda =>
{
    return{
        type: AkcijeSaNarudzbinama.CEKIRANJE_PROIZVODA,
        stavkaKojaSeDodaje: stavkaRacuna
    }
}

//moram da regulisem kad ce ovo da se dodaje, tj kad se "odcekira" nesto moram ovo da dispecujem
export const A_OdcekiranjeProizvoda = (stavkaRacuna: StavkaRacuna) : OdcekiranjeProizvoda =>
{
    return{
        type: AkcijeSaNarudzbinama.ODCEKIRANJE_PROIZVODA,
        stavkaRacuna: stavkaRacuna
    }
}

export const A_PromeniKolicinuProizvoda = (stavka: StavkaRacuna) : PromeniKolicinuProizvoda =>
{
    return{
        type: AkcijeSaNarudzbinama.PROMENI_KOLICINU_PROIZVODA,
        stavka: stavka
    }
}

export const A_PotvrdiNarudzbinu = (stavke: StavkaRacuna[]): PotvrdiNarudzbinu => {
    return {
        type: AkcijeSaNarudzbinama.POTVRDI_NARUDZBINU,
        stavkeNaNarudzbini: stavke
    }
}