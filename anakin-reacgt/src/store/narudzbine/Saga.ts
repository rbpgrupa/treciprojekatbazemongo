import * as Saga from 'redux-saga/effects';
import { AkcijeSaNarudzbinama, CekiranjeProizvoda, OdcekiranjeProizvoda, PromeniKolicinuProizvoda, PotvrdiNarudzbinu } from './Modeli';

const putanjaDoNarudzbina = "http://localhost:3004/dodajRacun" ; //fake baza mora se napravi sada samo da imam putanju

export function* rootSagaZaNarudzbine() 
{
    yield Saga.all([Saga.fork(posmatrajZahteveNarudzbina)]);
}

function* posmatrajZahteveNarudzbina()
{
    yield Saga.takeEvery(AkcijeSaNarudzbinama.POTVRDI_NARUDZBINU, dodajNarudzbinuUBazu);
}

function* dodajNarudzbinuUBazu(akcija: PotvrdiNarudzbinu)
{
    const { stavkeNaNarudzbini } = akcija;//saljem samo listu id-eva
    interface ParID_Cena { idProizvoda: string, kolicina: number };
    
    let onoStoSaljemUBazu: ParID_Cena[] = stavkeNaNarudzbini.map(stavka => {
        const povratnaVrednost: ParID_Cena = {
            idProizvoda: stavka.narucenProizvod._id,
            kolicina: stavka.kolicina
        };
        return povratnaVrednost;
    });

    yield uputiZahtevKaBazi("POST", putanjaDoNarudzbina, onoStoSaljemUBazu);
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)//podaci su opcioni, valjda ovako ide
{
    let HTTPZahtev: RequestInit = {
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, /'
        }
    };

    if(podaci)
        HTTPZahtev.body = JSON.stringify(podaci);

    let ishodFetcha = yield fetch(URL, HTTPZahtev);

    if(metoda !== "POST")
        return yield ishodFetcha.json();
}