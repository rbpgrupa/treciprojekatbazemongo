import { StavkaRacuna } from "../../models/StavkaRacuna";


export enum AkcijeSaNarudzbinama
{
    CEKIRANJE_PROIZVODA="[Akcije sa narudzbinom] Dodavanje stavke narudzbine",
    ODCEKIRANJE_PROIZVODA="[Akcije sa narudzbinom] Brisanje stavke narudzbine",
    PROMENI_KOLICINU_PROIZVODA="[Akcija sa narudzbinom] Menjanje kolicine proizvoda",
    POTVRDI_NARUDZBINU = "[Akcije sa narudzbinama] Potvrdi narudzbinu"
}

export interface CekiranjeProizvoda
{
    type:AkcijeSaNarudzbinama,
    stavkaKojaSeDodaje: StavkaRacuna
}

export interface OdcekiranjeProizvoda
{
    type: AkcijeSaNarudzbinama,
    stavkaRacuna: StavkaRacuna
}

export interface PromeniKolicinuProizvoda
{
    type: AkcijeSaNarudzbinama,
    stavka: StavkaRacuna //kolicina je wrapp-ovana u ovome
}

export interface PotvrdiNarudzbinu
{
    type: AkcijeSaNarudzbinama,
    stavkeNaNarudzbini: StavkaRacuna[]
}