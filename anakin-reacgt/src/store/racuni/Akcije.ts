import { UcitajRacune, AkcijeRacuni, ProslediRacune } from "./Modeli";
import { CeoRacun } from "../../models/CeoRacun";

export const A_UcitajRacune = (username: string, token: string): UcitajRacune => {
    return {
        type: AkcijeRacuni.UCITAJ_SVE_RACUNE,
        username: username,
        token: token
    }
}

export const A_ProslediRacune = (racuni: CeoRacun[]): ProslediRacune => {
    return {
        type: AkcijeRacuni.PROSLEDI_RACUNE,
        racuni: racuni
    }
}