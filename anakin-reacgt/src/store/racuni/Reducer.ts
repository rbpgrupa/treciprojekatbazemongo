import { CeoRacun } from "../../models/CeoRacun";
import { Action } from "redux";
import { AkcijeRacuni, ProslediRacune } from "./Modeli";

export interface InformacijeORacunima
{
    racuni: CeoRacun[],
    racuniSeUcitavaju: boolean
}

const pocetnoStanje: InformacijeORacunima = {
    racuni: [],
    racuniSeUcitavaju: true
};

export default function RacuniReducer(stanje = pocetnoStanje, akcija: Action<any>): InformacijeORacunima
{
    switch(akcija.type)
    {
        case AkcijeRacuni.PROSLEDI_RACUNE:
        {
            const { racuni } = akcija as ProslediRacune;
            return {
                ...stanje,
                racuni: racuni,
                racuniSeUcitavaju: false
            }
        }

        default:
            return stanje;
    }
}