import { CeoRacun } from "../../models/CeoRacun";

export enum AkcijeRacuni
{
    UCITAJ_SVE_RACUNE = "[Akcije racuni] Ucitaj sve racune",
    PROSLEDI_RACUNE = "[Akcije racuni] Prosledi racune"
}

export interface UcitajRacune
{
    type: AkcijeRacuni,
    username: string,
    token: string
}

export interface ProslediRacune
{
    type: AkcijeRacuni,
    racuni: CeoRacun[]
}