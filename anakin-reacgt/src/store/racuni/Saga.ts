import * as Saga from 'redux-saga/effects';
import { AkcijeRacuni, UcitajRacune } from './Modeli';
import { A_ProslediRacune } from './Akcije';

const putanjaDoRacuna: string  = "http://localhost:3004/vratiRacune";

export function* rootSagaZaRacune()
{
    yield Saga.all([Saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve()
{
    yield Saga.takeEvery(AkcijeRacuni.UCITAJ_SVE_RACUNE, ucitajRacune);
}

function* ucitajRacune(akcija: UcitajRacune)
{
    const { token, username } = akcija;
    let objekatZahteva = {
        token,
        username
    };

    let racuni = yield uputiZahtevKaBazi("POST", putanjaDoRacuna, objekatZahteva);
    //error handling nikakav
    yield Saga.put(A_ProslediRacune(racuni));
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)
{
    let HTTPZahtev: RequestInit = {
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, /'
        }
    };

    if(podaci)
        HTTPZahtev.body = JSON.stringify(podaci);

    let ishodFetcha = yield fetch(URL, HTTPZahtev);

    if(metoda !== "DELETE" /*&& metoda !== "POST"*/)
        return yield ishodFetcha.json();
}