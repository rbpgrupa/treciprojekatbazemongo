import { Proizvod } from "../../models/Proizvod";
import { Action } from 'redux';
import { AkcijeSaProizvodima, BrisanjeProizvoda, ProslediProizvodeUspeh, ProslediProizvodeGreska, DodavanjeProizvoda } from "./Modeli";

export interface InformacijeOProizvodima { //reducer gura podatke meniju, a meni proizvodview
    proizvodi: Proizvod[],
    proizvodiSeUcitavaju: boolean,
    greskaPrilikomProsledjivanja: boolean
}

const pocetnoStanje: InformacijeOProizvodima = 
{
    proizvodi: [],
    proizvodiSeUcitavaju: true, //dosta bitno za tzv "uslovno renderovanje", kako bi korisnik znao da se proizvodi ucitavaju
    greskaPrilikomProsledjivanja: false
}

export default function ReducerProizvoda(stanje=pocetnoStanje, akcija: Action<any>) : InformacijeOProizvodima
{
    switch(akcija.type)
    {
        case AkcijeSaProizvodima.ZAHTEV_ZA_UCITAVANJE: //glavni deo ove akcije odradjuje saga, ali sinhroni deo, tj ucitavanje ekrana, moze da se hendlluje ovde
        {
            return {
                ...stanje,
                proizvodiSeUcitavaju: true
            }
        }

        case AkcijeSaProizvodima.BRISANJE_PROIZVODA:
        {
            const { idProizvoda } = akcija as BrisanjeProizvoda;
            return {
                ...stanje,
                proizvodi: stanje.proizvodi.filter (proizvod => proizvod._id !== idProizvoda)
            }
        }

        case AkcijeSaProizvodima.PROSLEDI_PROIZVODE_USPEH:
        {
            const { proizvodi } = akcija as ProslediProizvodeUspeh;
            return{
                ...stanje,
                proizvodi: proizvodi,
                proizvodiSeUcitavaju: false
            }
        }

        case AkcijeSaProizvodima.PROSLEDI_PROIZVODE_GRESKA:
        {
            const { greska } = akcija as ProslediProizvodeGreska;
            return {
                ...stanje,
                greskaPrilikomProsledjivanja: greska
            }
        }    

        case AkcijeSaProizvodima.DODAVANJE_PROIZVODA:
        {
            const { zahtev } = akcija as DodavanjeProizvoda;
            return {
                ...stanje,
                proizvodi: [...stanje.proizvodi, zahtev.proizvod]
            }
        }

        default: return stanje;
    }
}