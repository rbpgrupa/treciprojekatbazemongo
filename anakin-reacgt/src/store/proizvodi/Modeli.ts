import { Proizvod } from "../../models/Proizvod";
import { ZahtevDodavanjaProizvoda } from "../../models/ZahtevDodavanjeProizvoda";

export enum AkcijeSaProizvodima 
{
    ZAHTEV_ZA_UCITAVANJE="[Akcije sa proizvodima] Zahtev za ucitavanje proizvoda",
    PROSLEDI_PROIZVODE_USPEH="[Akcije sa proizvodima] Prosledi proizvode uspeh",
    PROSLEDI_PROIZVODE_GRESKA="[Akcije sa proizvodima] Prosledi proizvode greska",
    BRISANJE_PROIZVODA="[Akcije sa proizvodima] Brisanje proizvoda",
    DODAVANJE_PROIZVODA="[Akcije sa proizvodima] Dodavanje proizvoda",
    DODAVANJE_AKCIJE_PROIZVODU = "[Akcije sa proizvodima] Dodaj akciju proizvodu"
}

export interface DodavanjeProizvoda
{
    type:AkcijeSaProizvodima,
    zahtev: ZahtevDodavanjaProizvoda
}

export interface ZahtevZaUcitavanje 
{
    type: AkcijeSaProizvodima,
    kategorija: string
}

export interface ProslediProizvodeUspeh 
{
    type: AkcijeSaProizvodima,
    proizvodi : Proizvod[]
}

export interface ProslediProizvodeGreska 
{
    type: AkcijeSaProizvodima,
    greska: boolean
}

export interface BrisanjeProizvoda 
{
    type: AkcijeSaProizvodima,
    idProizvoda: string,
    username: string,
    token: string
}

export interface DodavanjeAkcijeProizvodu
{
    type: AkcijeSaProizvodima,
    idProizvoda: string,
    username: string;
    token: string,
    akcija: number
}