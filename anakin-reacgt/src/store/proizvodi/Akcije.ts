import { ZahtevZaUcitavanje, AkcijeSaProizvodima, ProslediProizvodeUspeh, ProslediProizvodeGreska, BrisanjeProizvoda, DodavanjeProizvoda, DodavanjeAkcijeProizvodu } from "./Modeli"
import { Proizvod } from "../../models/Proizvod"
import { ZahtevDodavanjaProizvoda } from "../../models/ZahtevDodavanjeProizvoda"

export const A_ZahtevZaUcitavanje = (kategorija: string) : ZahtevZaUcitavanje =>
{
    return {
        type: AkcijeSaProizvodima.ZAHTEV_ZA_UCITAVANJE,
        kategorija: kategorija
    }
}

export const A_ProslediProizvodeUspeh = (proizvodi:Proizvod[]) : ProslediProizvodeUspeh =>
{
    return {
        type:AkcijeSaProizvodima.PROSLEDI_PROIZVODE_USPEH,
        proizvodi:proizvodi
    }
}

export const A_ProslediProizvodeGreska = (greska: boolean) : ProslediProizvodeGreska =>
{
    return {
        type:AkcijeSaProizvodima.PROSLEDI_PROIZVODE_GRESKA,
        greska: greska
    }
}

export const A_BrisanjeProizvoda = (idProizvoda: string, username: string, token: string) : BrisanjeProizvoda => //dodao sam ovde akciju brisanje proizvoda to smo zaboravili
{
    return {
        type: AkcijeSaProizvodima.BRISANJE_PROIZVODA,
        idProizvoda: idProizvoda,
        username: username,
        token: token
    }
}

export const A_DodavanjeProizvoda = (noviZahtev : ZahtevDodavanjaProizvoda) : DodavanjeProizvoda => // u modelu sam naveo zasto smo ovu akciju dodali
{
    return {
        type: AkcijeSaProizvodima.DODAVANJE_PROIZVODA,
        zahtev: noviZahtev
    }
}

export const A_DodavanjeAkcijeProizvodu = (idProizvoda: string,
    username: string,
    token: string,
    iznosAkcije: number): DodavanjeAkcijeProizvodu => {
        return {
            type: AkcijeSaProizvodima.DODAVANJE_AKCIJE_PROIZVODU,
            idProizvoda: idProizvoda,
            akcija: iznosAkcije,
            token: token,
            username: username
        }
    }