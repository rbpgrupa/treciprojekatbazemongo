import * as Saga from 'redux-saga/effects';
import { AkcijeSaProizvodima, DodavanjeProizvoda, BrisanjeProizvoda, ZahtevZaUcitavanje, DodavanjeAkcijeProizvodu } from './Modeli';
import { Proizvod } from '../../models/Proizvod';
import { A_ProslediProizvodeUspeh } from './Akcije';


const putanjaDoProizvoda: string = "http://localhost:3004/vratiProizvode";
const putanjaObrisi: string = "http://localhost:3004/obrisiProizvod";
const putanjaDodaj: string = "http://localhost:3004/dodajProizvod";
const dodajAkcijuProizvodu: string = "http://localhost:3004/izmeniAkciju";

export function* rootSagaZaProizvode() 
{
    yield Saga.all([Saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve() // ovde pisemo akcije koje SAMO BAZA SLUSA
{
    yield Saga.takeEvery(AkcijeSaProizvodima.ZAHTEV_ZA_UCITAVANJE, zahtevZaUcitavanjeProizvoda);
    yield Saga.takeEvery(AkcijeSaProizvodima.DODAVANJE_PROIZVODA, dodajProizvodUBazu);
    yield Saga.takeEvery(AkcijeSaProizvodima.BRISANJE_PROIZVODA, brisiProizvodIzBaze);
    yield Saga.takeEvery(AkcijeSaProizvodima.DODAVANJE_AKCIJE_PROIZVODU, dodajAkciju);
}

function* dodajAkciju(akkcija: DodavanjeAkcijeProizvodu)
{
    const { idProizvoda, username, token, akcija } = akkcija;
    console.table(akcija);
    let objekatKomSeDodajeAkcija = {
        idProizvoda,
        username,
        token,
        akcija: akcija
    };

    yield uputiZahtevKaBazi("POST", dodajAkcijuProizvodu, objekatKomSeDodajeAkcija);
}

function* zahtevZaUcitavanjeProizvoda(akcija: ZahtevZaUcitavanje)
{
    const { kategorija } = akcija;
    console.log(kategorija);
    const filterZaNode = {
        tip: kategorija
    };

    const proizvodi:Proizvod[] = yield uputiZahtevKaBazi("POST", putanjaDoProizvoda, filterZaNode);
    console.log(proizvodi);
    yield Saga.put(A_ProslediProizvodeUspeh(proizvodi));
}

function* brisiProizvodIzBaze(akcija: BrisanjeProizvoda)
{
    const { idProizvoda, username, token } = akcija; //ovo moram da popravim
    let objekatZahtevaZaBrisanje = {
        idProizvoda,
        username,
        token
    };

    yield uputiZahtevKaBazi("POST", putanjaObrisi, objekatZahtevaZaBrisanje);
} 

function* dodajProizvodUBazu(akcija: DodavanjeProizvoda)
{
    const { zahtev } = akcija;
    yield uputiZahtevKaBazi("POST", putanjaDodaj, zahtev);
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)
{
    let HTTPZahtev: RequestInit = {
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, /'
        }
    };

    if(podaci)
        HTTPZahtev.body = JSON.stringify(podaci);

    let ishodFetcha = yield fetch(URL, HTTPZahtev);

    if(metoda !== "DELETE" /*&& metoda !== "POST"*/)
        return yield ishodFetcha.json();
}