import ReducerProizvoda, { InformacijeOProizvodima } from "./proizvodi/Reducer";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { rootSagaZaProizvode } from "./proizvodi/Saga";
import { all } from "redux-saga/effects";
import createSagaMiddleware, { SagaMiddleware } from '@redux-saga/core';
import { composeWithDevTools } from 'redux-devtools-extension'
import ReducerAdmina, { InformacijeAdmin } from "./admini/Reducer";
import { RootSagaZaAdmine } from "./admini/Saga";
import ReducerNarudzbine, { InformacijeONarduzbini } from "./narudzbine/Reducer";
import RacuniReducer, { InformacijeORacunima } from "./racuni/Reducer";
import { rootSagaZaNarudzbine } from "./narudzbine/Saga";
import { rootSagaZaRacune } from "./racuni/Saga";


export interface RootStanje
{
    stanjeProizvoda: InformacijeOProizvodima,
    informacijeAdmin: InformacijeAdmin,
    stanjeNarudzbine: InformacijeONarduzbini,
    informacijeRacuni: InformacijeORacunima
}

const rootReducer= combineReducers ({
    stanjeProizvoda: ReducerProizvoda,
    informacijeAdmin: ReducerAdmina,
    stanjeNarudzbine: ReducerNarudzbine,
    informacijeRacuni: RacuniReducer
})

function* rootSaga()
{
    yield all([
        rootSagaZaProizvode(),
        RootSagaZaAdmine(),
        rootSagaZaNarudzbine(),
        rootSagaZaRacune()
    ]);
}

export default function konfigurisiStore()
{
    const sagaMiddleware: SagaMiddleware = createSagaMiddleware();
    const store=createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );
    sagaMiddleware.run(rootSaga);
    return store;
}