import { Admin } from "../../models/Admin";
import { LoginGreske } from "../../models/LoginGreske";

export enum AdminAkcije
{
    PRIJAVLJIVANJE_ADMINA = "[Admin akcije] Prijavljivanje admina",
    PRIJAVLJIVANJE_ADMINA_USPEH = "[Admin akcije] Prijjavljivanje amina uspeh",
    PRIJAVLJIVANJE_ADMINA_GRESKA = "[Admin akcije] Prijavljivanje admina greska",
    ODJAVI_ADMINA = "[Admin akcije] Odjavi admina"
}

export interface PrijavljivanjeAdmina
{
    type: AdminAkcije,
    loginPodaci: Admin
}

export interface PrijavljivanjeAdminaGreska
{
    type: AdminAkcije,
    greske: LoginGreske
}

export interface PrijavljivanjeAdminaUspeh //ovo resetuje sve greske na false
{
    type: AdminAkcije,
    admin: Admin
}

export interface OdjaviAdmina
{
    type: AdminAkcije
}