import { PrijavljivanjeAdmina, AdminAkcije, PrijavljivanjeAdminaGreska, PrijavljivanjeAdminaUspeh, OdjaviAdmina } from "./Modeli";
import { Admin } from "../../models/Admin";
import { LoginGreske } from "../../models/LoginGreske";

export const A_PrijavljivanjeAdmina = (loginPodaci: Admin): PrijavljivanjeAdmina => {
    return {
        type: AdminAkcije.PRIJAVLJIVANJE_ADMINA,
        loginPodaci: loginPodaci
    };
};

export const A_PrijavljivanjeAdminaGreska = (objekatGreske: LoginGreske): PrijavljivanjeAdminaGreska => {
    return {
        type: AdminAkcije.PRIJAVLJIVANJE_ADMINA_GRESKA,
        greske: objekatGreske
    };
};

export const A_PrijavljivanjeAdminaUspeh = (admin: Admin): PrijavljivanjeAdminaUspeh => {
    return {
        type: AdminAkcije.PRIJAVLJIVANJE_ADMINA_USPEH,
        admin: admin
    };
};

export const A_OdjaviAdmina = (): OdjaviAdmina => {
    return {
        type: AdminAkcije.ODJAVI_ADMINA
    };
};