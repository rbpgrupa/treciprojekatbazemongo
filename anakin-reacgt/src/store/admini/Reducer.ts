import { Admin } from "../../models/Admin";
import { Action } from "redux";
import { AdminAkcije, PrijavljivanjeAdminaGreska, PrijavljivanjeAdminaUspeh } from "./Modeli";
import { LoginGreske } from "../../models/LoginGreske";

export interface InformacijeAdmin
{
    adminPodaci: Admin,
    prijavaSeObradjuje: boolean,
    greske: LoginGreske,
    adminJePrijavljen: boolean
}

const pocetnoStanje: InformacijeAdmin = {
    adminPodaci: {
        username: "",
        password: "",
        token: ""
    },
    prijavaSeObradjuje: false,
    greske: {
        usernameNePostoji: false,
        passwordJePogresan: false,
        tokenNeValja: false
    },
    adminJePrijavljen: false
};

export default function ReducerAdmina(stanje = pocetnoStanje, akcija: Action<any>): InformacijeAdmin
{
    switch(akcija.type)
    {
        case AdminAkcije.PRIJAVLJIVANJE_ADMINA:
        {
            return {
                ...stanje,
                prijavaSeObradjuje: true //ovo je za loading screen
            };
        }

        case AdminAkcije.PRIJAVLJIVANJE_ADMINA_GRESKA:
        {
            const { greske } = akcija as PrijavljivanjeAdminaGreska;
            return {
                ...stanje,
                prijavaSeObradjuje: false,
                greske: greske,
                adminJePrijavljen: false
            };
        }

        case AdminAkcije.PRIJAVLJIVANJE_ADMINA_USPEH:
        {
            const { admin } = akcija as PrijavljivanjeAdminaUspeh;
            return {
                ...stanje,
                adminPodaci: admin,
                prijavaSeObradjuje: false,
                greske: {
                    usernameNePostoji: false,
                    passwordJePogresan: false,
                    tokenNeValja: false
                },
                adminJePrijavljen: true
            };
        }

        case AdminAkcije.ODJAVI_ADMINA:
        {
            return {
                ...stanje,
                adminPodaci: {
                    username: "",
                    password: "",
                    token: ""
                },
                prijavaSeObradjuje: false,
                greske: {
                    usernameNePostoji: false,
                    passwordJePogresan: false,
                    tokenNeValja: false
                },
                adminJePrijavljen: false
            }
        }

        default:
            return stanje;
    }
}