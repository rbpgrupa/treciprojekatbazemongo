import * as Saga from 'redux-saga/effects';
import { AdminAkcije, PrijavljivanjeAdmina } from './Modeli';
import { Admin } from '../../models/Admin';
import { A_PrijavljivanjeAdminaGreska, A_PrijavljivanjeAdminaUspeh } from './Akcije';
import { LoginGreske } from '../../models/LoginGreske';
import { ResponseLogovanjaServer } from '../../models/ResponseLogovanjaServer';

const putanjaDoAdmina: string = "http://localhost:3004/ulogujAdmina";//sad vec prava baza

export function* RootSagaZaAdmine()
{
    yield Saga.all([Saga.fork(PosmatrajZahteve)]);
}

function* PosmatrajZahteve()
{
    yield Saga.takeEvery(AdminAkcije.PRIJAVLJIVANJE_ADMINA, prijavljivanjeAdmina);
}

function* prijavljivanjeAdmina(akcija: PrijavljivanjeAdmina)
{
    const { loginPodaci } = akcija;
    //prvo hvatam admina iz baze na osnovu korisnickog imena, onda gledam da vidim sta ima od podataka
    let ishodFetcha: ResponseLogovanjaServer = yield uputiZahtevKaBazi("POST", `${putanjaDoAdmina}`, loginPodaci);
    console.log(ishodFetcha);

    let objekatGreske: LoginGreske = {
        usernameNePostoji: false,
        passwordJePogresan: false,
        tokenNeValja: false
    };

    if(ishodFetcha.greske.usernameNePostoji)
    {
        objekatGreske.usernameNePostoji = true;
        yield Saga.put(A_PrijavljivanjeAdminaGreska(objekatGreske));
    }
    else
        if(ishodFetcha.greske.passwordJePogresan)
        {
            objekatGreske.passwordJePogresan = true;
            yield Saga.put(A_PrijavljivanjeAdminaGreska(objekatGreske));
        }
        else //inace je sve u redu
        {
            loginPodaci.token = ishodFetcha.token;
            yield Saga.put(A_PrijavljivanjeAdminaUspeh(loginPodaci));    
        }
    //ishodFetcha je u ovom slucaju admin, sad trebam da vidim dal je dobro korisnicko ime i dal dobra sifra
    //i na osnovu toga ja bacam konkretne akcije i greske
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)
{
    let HTTPZahtev: RequestInit = {
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, /'
        }
    };

    if(podaci)
        HTTPZahtev.body = JSON.stringify(podaci);

    let ishodFetcha = yield fetch(URL, HTTPZahtev);
    return yield ishodFetcha.json();
}