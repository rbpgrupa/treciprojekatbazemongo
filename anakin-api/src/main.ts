import {Kontroler} from './kontroler';
import * as express from 'express';
import * as bodyparser from 'body-parser';
import * as cors from 'cors';

let kon = new Kontroler('mongodb://localhost:27017/anakin');
const port = 3004;

let api = express();
api.use(bodyparser.json());
api.use(cors());

// --- RUTE ---
// uloguj admina
api.post('/ulogujAdmina', async function(req, res) {
    let ret = await kon.ulogujAdmina(req.body.username, req.body.password);
    res.send(JSON.stringify(ret));
});
// dodaj proizvod
api.post('/dodajProizvod', async function(req, res) {
    let ret = await kon.dodajProizvod(req.body.proizvod, req.body.username, req.body.token);
    res.send(JSON.stringify(ret));
});
// izmeni akciju
api.post('/izmeniAkciju', async function(req, res) {
    let ret = await kon.izmeniAkciju(req.body.idProizvoda, req.body.akcija, req.body.username, req.body.token);
    res.send(JSON.stringify(ret));
});
// obrisi proizvod
api.post('/obrisiProizvod', async function(req, res) {
    let ret = await kon.obrisiProizvod(req.body.idProizvoda, req.body.username, req.body.token);
    res.send(JSON.stringify(ret));
});
// vrati proizvod odredjenog tipa
api.post('/vratiProizvode', async function(req, res) {
    let ret = await kon.vratiProizvode(req.body.tip);
    res.send(JSON.stringify(ret));
});
// dodaj racun u bazu
api.post('/dodajRacun', async function(req, res) {
    console.table(req.body);
    let ret = await kon.dodajRacun(req.body);
    res.send("poslato");
});
// vrati racune
api.post('/vratiRacune', async function(req, res) {
    let ret = await kon.vratiRacune(req.body.username, req.body.token);
    res.send(ret);
});


api.listen(port);
console.log("Server je pokrenut na portu: " + port);