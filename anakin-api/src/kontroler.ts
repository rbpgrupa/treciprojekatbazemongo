import * as mongoose from 'mongoose';
import * as proizvod from './proizvod';
import * as admin from './admin';
import * as racun from './racun';

/*
    Autentikacija funkcionise ovako:

    Kod god se neko uloguje sa valdinim usernameom i sifrom
    server generise token (string duzine 16) za tog korisnika.
    Jedna kopija ide u bazu a druga se salje klijentu.
    Kad god treba neka admin operacija da se obavi
    klijent salje (uz zahtev) svoj username i token i server proverava da li
    je taj token validan za taj username.
*/

interface IKontroler {
    genToken(): string // generise 64-bitni token (string od 16 hex cifara)
    proveriToken(username: string, token: string): Promise<boolean>; // proveri da li je admin token validan
    ulogujAdmina(username: string, password: string): Promise<object>; // proveri username i pass i ako su odgovarajuci, generisi token

    dodajProizvod(p: proizvod.IProizvod, username: string, token: string): Promise<object>; // dodaj proizvod u bazu
    izmeniAkciju(naziv: string, nova: number, username: string, token: string): Promise<object>; // postavi (ili skloni) proizvod sa datim imenom sa akcije
    obrisiProizvod(naziv: string, username: string, token: string): Promise<object>; // obrisi proizvod sa datim nazivom

    vratiProizvode(filter: string): Promise<object>; // vrati sve proizvode datog tipa

    dodajRacun(racuni: any[]): Promise<void>; // dodaj racun u bazu
    vratiRacune(username: string, token: string): Promise<object>; // vrati sve racune iz baze
}

export class Kontroler implements IKontroler {
    url: string;
    db: mongoose.Connection;
    pModel: mongoose.Model<proizvod.IProizvod, {}>; // model proizvoda
    aModel: mongoose.Model<admin.IAdmin, {}>; // model admina
    rModel: mongoose.Model<racun.IRacun, {}>; // model racuna

    constructor(url: string) {
        mongoose.set('useUnifiedTopology', true);
        mongoose.set('useNewUrlParser', true);

        this.url = url;
        mongoose.connect(this.url);
        this.db = mongoose.connection;
        this.db.on('error', console.error.bind(console, 'Greska pri konekciji:'));  

        this.pModel = mongoose.model<proizvod.IProizvod>('proizvod', proizvod.ProizvodSchema);
        this.aModel = mongoose.model<admin.IAdmin>('admin', admin.AdminSchema);
        this.rModel = mongoose.model<racun.IRacun>('racun', racun.RacunSchema);
        
    }

    genToken(): string {
        let ret = '';
        for (let i = 0; i < 16; i++) {
            ret += Math.floor(Math.random() * 16).toString(16);
        }
        return ret;
    }

    async proveriToken(username: string, token: string): Promise<boolean> {
        let ret: boolean;
        let doc = await this.aModel.findOne({username: username, token: token})
        if (doc)
            ret = true;
        else
            ret = false;
        return ret;
    }

    async ulogujAdmina(username: string, password: string): Promise<object> {
        let ret = {
            greske: {
                usernameNePostoji: true, 
                passwordJePogresan: true
            }, 
            token: ''
        };

        let doc = await this.aModel.findOne({username: username})
        if (doc) {
            ret.greske.usernameNePostoji = false;
            if (doc.password === password) {
                ret.greske.passwordJePogresan = false;
                ret.token = this.genToken();
                doc.token = ret.token;
                doc.save();
            }
        }
        
        return ret;
    }

    async dodajProizvod(p: any, username: string, token: string): Promise<object> {   
        let ret = {greske: {tokenNeValja: true}};
        const np = {naziv: p.naziv, tip: p.tip, cena: p.cena};
        if (await this.proveriToken(username, token)) {
            let doc = new this.pModel(np);
            doc.save().catch(function(err) {
                console.log(err.message);
            });
            ret.greske.tokenNeValja = false;
        }
            
        return ret;
    }

    async izmeniAkciju(id: string, nova: number, username: string, token: string): Promise<object> {
        let ret = {greske: {tokenNeValja: true}};
        if (await this.proveriToken(username, token)) {
            let doc = await this.pModel.findOne({_id: mongoose.Types.ObjectId(id)});
            doc.akcija = nova;
            doc.save();
            ret.greske.tokenNeValja = false;
        }

        return ret;
    }

    async obrisiProizvod(id: string, username: string, token: string): Promise<object> {
        let ret = {greske: {tokenNeValja: true}};
        if (await this.proveriToken(username, token)) {
            await this.pModel.deleteOne({_id: mongoose.Types.ObjectId(id)});
            ret.greske.tokenNeValja = false;
        }
        return ret;
    }

    async vratiProizvode(filter: string): Promise<object> {
        return await this.pModel.find({tip: filter});
    }

    async dodajRacun(racuni: any[]): Promise<void> {
        let telo = '';
        let ukupnaCena = 0;
        for (let i = 0; i < racuni.length; i++) {
            let doc = await this.pModel.findOne({_id: racuni[i].idProizvoda});
            let cena = doc.cenaSaAkcijom() * racuni[i].kolicina;
            telo += doc.naziv + ' ' + ' x' + racuni[i].kolicina + ' ' + cena + 'din.\n';
            ukupnaCena += cena;
        }
        let datum = new Date();

        let racun = new this.rModel({telo: telo, cena: ukupnaCena, datum: datum.toString()});
        racun.save().catch(function(err) {
            console.log(err.message);
        });
    }

    async vratiRacune(username: string, token: string): Promise<object> {
        let ret: any;
        ret = {greske: {tokenNeValja: true}};
        if (await this.proveriToken(username, token)) {
            ret = await this.rModel.find({});
        }
            
        return ret;
    }
}  