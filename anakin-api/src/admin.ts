import * as mongoose from 'mongoose';

export interface IAdmin extends mongoose.Document {
    username: string;
    password: string;
    token: string;
}

export let AdminSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Nije dat username.'],
    },
    password: {
        type: String,
        required: [true, 'Nije dat password.']
    },
    token: {
        type: String,
        default: "0"
    },
}, {collection: 'admin'});