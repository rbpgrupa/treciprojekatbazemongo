import * as mongoose from 'mongoose';
import * as proizvod from './proizvod';
import * as admin from './admin';

mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);

mongoose.connect('mongodb://localhost:27017/anakin');
let db = mongoose.connection;

let pModel = mongoose.model<proizvod.IProizvod>('proizvod', proizvod.ProizvodSchema);
let aModel = mongoose.model<admin.IAdmin>('admin', admin.AdminSchema);

//PROIZVODI
async function popuni() {
    let doc = new pModel({naziv: 'Kafa', cena: 100, tip: 'Topli napitak'});
    await doc.save();
    doc = new pModel({naziv: 'Esspreso', cena: 130, tip: 'Topli napitak'});
    await doc.save();
    doc = new pModel({naziv: 'Caj', cena: 100, tip: 'Topli napitak', akcija: 20});
    await doc.save();
    doc = new pModel({naziv: 'Topla cokolada', cena: 110, tip: 'Topli napitak'});
    await doc.save();
    doc = new pModel({naziv: 'Kuvana rakija', cena: 180, tip: 'Topli napitak'});
    await doc.save();

    doc = new pModel({naziv: 'Limunada', cena: 110, tip: 'Hladni napitak', akcija: 33});
    await doc.save();
    doc = new pModel({naziv: 'Pivo', cena: 130, tip: 'Hladni napitak'});
    await doc.save();
    doc = new pModel({naziv: 'Coca Cola', cena: 140, tip: 'Hladni napitak', akcija: 10});
    await doc.save();
    doc = new pModel({naziv: 'Hladni nes', cena: 100, tip: 'Hladni napitak'});
    await doc.save();

    doc = new pModel({naziv: 'Ekler', cena: 120, tip: 'Kolac'});
    await doc.save();
    doc = new pModel({naziv: 'Krempita', cena: 140, tip: 'Kolac', akcija: 20});
    await doc.save();
    doc = new pModel({naziv: 'Princes krofna', cena: 110, tip: 'Kolac'});
    await doc.save();
    doc = new pModel({naziv: 'Cheesecake', cena: 150, tip: 'Kolac'});
    await doc.save();

    //ADMIN
    let doc2 = new aModel({username: 'root', password: 'admin'});
    await doc2.save();

    db.close();

}

popuni();


