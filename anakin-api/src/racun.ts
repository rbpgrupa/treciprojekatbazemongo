import * as mongoose from 'mongoose';

export interface IRacun extends mongoose.Document {
    telo: string,
    cena: number,
    datum: string
}

export let RacunSchema = new mongoose.Schema({
    telo: {
        type: String,
        required: true
    },
    cena: {
        type: Number,
        required: true
    },
    datum: {
        type: String,
        required: true
    }
}, {collection: 'racun'});