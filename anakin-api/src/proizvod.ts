import * as mongoose from 'mongoose';

export interface IProizvod extends mongoose.Document {
    naziv: string;
    tip: string;
    cena: number;
    akcija?: number;
    cenaSaAkcijom(): number;
}

export let ProizvodSchema = new mongoose.Schema({
    naziv: {
        type: String,
        required: [true, 'Nije dat naziv proizvoda.']
    },
    tip: {
        type: String,
        required: [true, 'Nije dat tip proizvoda.']
    },
    cena: {
        type: Number,
        required: [true, 'Nije data cena proizvoda.']
    },
    akcija: {
        type: Number,
        default: 0
    }
}, {collection: 'proizvod'});

ProizvodSchema.methods.cenaSaAkcijom = function(): number {
    return this.cena * (100 - this.akcija) / 100;
};