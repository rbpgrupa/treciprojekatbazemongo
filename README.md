Potreban je instaliran mongo server i node.js + npm

## POKRETANJE SERVERA
1. pokrenuti mongo server na portu `27017`  
2. u folderu anakin-api pokrenuti `npm install`  
3. u istom folderu pokrenuti komandu `npx tsc`  
4. u novokreiranom folderu build pokrenuti `node popuni_bazu.js` da biste popunili bazu vrednostima  
5. u folderu build pokrenuti `node main.js`, server ce biti pokrenut na portu 3004  
6. admin login u klijentu je `user: root, pass: admin`  